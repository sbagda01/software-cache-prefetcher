#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdlib.h>

typedef struct node {
  int val;
  struct node * left;
  struct node * right;} node_t;

void insert(node_t * tree,int val);
void print_tree(node_t * current);
void printDFS(node_t * current);

int main() {
	node_t * test_list = malloc(sizeof(node_t));
	srand(time(NULL));   // should only be called once
	int r,i;      // returns a pseudo-random integer between 0 and RAND_MAX int i;
    for(i=0;i<10000;i++)
	{
		r=rand();
		insert(test_list,r);
	}
	printDFS(test_list);
}

void insert(node_t * tree,int val){
  if(tree->val=='\0')tree->val=val;
  else if(val<tree->val)
    if(tree->left!=NULL)insert(tree->left,val);
    else{
      tree->left=malloc(sizeof(node_t));
      tree->left->val=val;    }
  else if(val>=tree->val)
    if(tree->right!=NULL)insert(tree->right,val);
    else{
      tree->right=malloc(sizeof(node_t));
      tree->right->val=val;}    }

void print_tree(node_t * current) {
  if(current!=NULL)printf("\n%d ",current->val);
  if(current->left!=NULL) printf("L%d ",current->left->val);
  if(current->right!=NULL)printf(" R%d",current->right->val);
  if(current->left!=NULL) print_tree(current->left);
  if(current->right!=NULL)print_tree(current->right);}

void printDFS(node_t * current) {
  if(current->left!=NULL) printDFS(current->left);
  if(current!=NULL)printf("%d ",current->val);
  if(current->right!=NULL)printDFS(current->right);}

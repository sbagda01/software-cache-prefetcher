#include<stdio.h>
#include<stdlib.h>

#define MAX_NUMBERS 1000000
int  numbers[MAX_NUMBERS];


/*
 *	This functions tries to create 
 *	random access to the cache.
 * */

void access_numbers()
{
	int i;
	int r;
	for(i=0; i< MAX_NUMBERS; i++)
	{
		r=(int)rand()*MAX_NUMBERS;
		if(r<0)
			r=-r;
		numbers[r%(MAX_NUMBERS-2)]+=i;
	}

}

int main(int argc, char **argv)
{
	int i;
	for(i=0; i< MAX_NUMBERS; i++)
		numbers[i]=(int)rand();
	access_numbers();
	for(i=0; i< MAX_NUMBERS; i++)
		printf("%d \n",numbers[i]);
}

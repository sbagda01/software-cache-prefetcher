//
// Data Prefetching Championship Simulator 2
// Authors:
//	Sergiy Bagdasar
//	Carlos Escuin Blasco
//

#include <stdio.h>
#include "../inc/prefetcher.h"

#define NUM_OFFSETS 46
#define NUM_ENTRIES 64
#define TABLE_INDEX_SIZE 6
#define LINE_SIZE 6
#define SCORE_MAX 31
#define ROUND_MAX 100
#define BAD_SCORE_BIG_LLC 1
#define BAD_SCORE_SMALL_LLC 10
#define BW_LOW 64
#define BW 16
#define LOW_SCORE 20
#define ENTRIES_QUEUE 15
#define DELAY 60
#define MSHR_THRESHOLD_2 2
#define MSHR_THRESHOLD_12 12
#define GAUGE_MAX 8191
#define RATE_MAX 255

char prefetching; //If we prefetch or not
int bad_score; //If best_score <= bad_score we stop prefetching
int best_score; //Score of the current offset (best_offset)
int best_offset; //Current offset used for prefetching
int round; //Number of rounds in the offset learning
int offset_index; //Offset index
int offsets[NUM_OFFSETS] = {1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 8, -8, 9, -9, 10, -10, 12, -12, 15, -15, 16, -16, 18, -18, 20, -20, 24, -24, 25, -25, 27, -27, 30, -30, 32, -32, 36, -36, 40, -40, 45, -45, 48, -48}; //List of offsets
int scores[NUM_OFFSETS]; //List of scores of the offsets
unsigned long long int RRR_table[NUM_ENTRIES];
unsigned long long int LRR_table[NUM_ENTRIES];
int gauge;
int rate;
unsigned long long int last_cycle;
int bw;
int mshr_threshold;

char prefetch_bit[L2_SET_COUNT][L2_ASSOCIATIVITY];

//Delay Queue
struct delay_queue{
	unsigned long long int addr[ENTRIES_QUEUE];
	unsigned long long int cycle[ENTRIES_QUEUE];
	char valid[ENTRIES_QUEUE];
	int head;
	int tail;
}dq;


//Function that returns the index of the left bank of the 2-skewed associative cache
int get_left_index(unsigned long long int addr){
	int index = ((addr & (1 << 7)) >> 2) + ((addr & (1 << 9)) >> 5) + ((addr & (1 << 11)) >> 8);

	if(((addr >> 6) & 1) != ((addr >> 12) & 1)) index += 4;
	if(((addr >> 8) & 1) != ((addr >> 14) & 1)) index += 2;
	if(((addr >> 10) & 1) != ((addr >> 16) & 1)) index += 1;

	return index;
}

//Function that returns the index of the left bank of the 2-skewed associative cache
int get_right_index(unsigned long long int addr){
	int index = ((addr & (1 << 6)) >> 1)+ ((addr & (1 << 8)) >> 4) + ((addr & (1 << 10)) >> 7);

	if(((addr >> 7) & 1) != ((addr >> 13) & 1)) index += 4;
	if(((addr >> 9) & 1) != ((addr >> 15) & 1)) index += 2;
	if(((addr >> 11) & 1) != ((addr >> 17) & 1)) index += 1;

	return index;
}

void insert_delay_queue(unsigned long long int addr){
	
}
void l2_prefetcher_initialize(int cpu_num)
{
	printf("No Prefetching\n");
	// you can inspect these knob values from your code to see which configuration you're runnig in

	printf("Knobs visible from prefetcher: %d %d %d\n", knob_scramble_loads, knob_small_llc, knob_low_bandwidth);

	int i;
	for(i=0; i<NUM_ENTRIES; i++){
		RRR_table[i] = 0;
		LRR_table[i] = 0;
	}

	round = 1;
	offset_index = 0;
	for(i=0; i<NUM_OFFSETS; i++) scores[i] = 0;
	best_offset = offsets[0];

	prefetching = 1;
	best_score = 0;
	bad_score = BAD_SCORE_BIG_LLC;
	if(knob_small_llc) bad_score = BAD_SCORE_SMALL_LLC;

	int j;
	for(i=0; i<L2_SET_COUNT; i++)
		for(j=0; j<L2_ASSOCIATIVITY; j++)
			prefetch_bit[i][j] = 0;

	gauge = GAUGE_MAX/2;
	rate = 0;
	last_cycle = 0;

	bw = BW;
	if(knob_low_bandwidth) bw = BW_LOW;

	for(i=0; i<ENTRIES_QUEUE; i++){
		dq.addr[i] = 0;
		dq.valid[i] = 0;
		dq.cycle[i] = 0;
	}
	dq.head = 0;
	dq.tail = 0;
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr, unsigned long long int ip, int cache_hit)
{
  // uncomment this line to see all the information available to make prefetch decisions
  //printf("(0x%llx 0x%llx %d %d %d) ", addr, ip, cache_hit, get_l2_read_queue_occupancy(0), get_l2_mshr_occupancy(0));

	unsigned long long int cpu_cycle = get_current_cycle(cpu_num);
	int i;

	//Insert address from delay queue into the LRR table
	for(i=0; i<ENTRIES_QUEUE; i++){
		if(!dq.valid[(dq.head+i)%ENTRIES_QUEUE] || dq.cycle[(dq.head+i)%ENTRIES_QUEUE] > cpu_cycle) break;
		int left_index = get_left_index(dq.addr[(dq.head+i)%ENTRIES_QUEUE]);
		LRR_table[left_index] = dq.addr[(dq.head+i)%ENTRIES_QUEUE];
		dq.valid[(dq.head+i)%ENTRIES_QUEUE] = 0;
	}
	dq.head = (dq.head+i)%ENTRIES_QUEUE;

	//See if it is a prefetch hit
	char prefetch_hit = 0;
	if(cache_hit) {
		int set = l2_get_set(addr);
		int way = l2_get_way(cpu_num, addr, set);
		prefetch_hit = prefetch_bit[set][way];
		prefetch_bit[set][way] = 0;
	}

	//If L2 cache miss or prefetch_hit
	if(!cache_hit || prefetch_hit){
		unsigned long long int new_addr = ((addr>>LINE_SIZE)-offsets[offset_index])<<LINE_SIZE;
		int right_index = get_right_index(new_addr); //Right index to index right RR table
		int left_index = get_left_index(new_addr); //Left index to index left RR table

		//Update score and round
		if(RRR_table[right_index] == new_addr || LRR_table[left_index] == new_addr) scores[offset_index] += 1; //Is in right or left RR table?
		offset_index = (offset_index+1)%NUM_OFFSETS;

		if(offset_index == 0) round += 1; //Update round

		//End of phase?
		if(scores[offset_index] == SCORE_MAX || round == ROUND_MAX){
			best_score = scores[offset_index];
			best_offset = offsets[offset_index];
			for(i=NUM_OFFSETS-1; i>=0; i--){ //Find the best offset
				if(scores[i] > best_score){
					best_score = scores[i];
					best_offset = offsets[i];
				}
				scores[i] = 0; //Reset scores
			}
			prefetching = 1;
			if(best_score <= bad_score) prefetching = 0;
			round = 1; //Reset round count
		}

		//Issue prefetch
		new_addr = ((addr>>LINE_SIZE)+best_offset)<<LINE_SIZE;
		char same_page = (((new_addr) ^ (addr)) >> 12) == 0; // 4 KB Page
		char prefetched = 0;
		if(same_page && prefetching && mshr_threshold > get_l2_mshr_occupancy(cpu_num)) prefetched = l2_prefetch_line(cpu_num, addr, new_addr, FILL_L2);
		else if(same_page && prefetching && best_score > LOW_SCORE) prefetched = l2_prefetch_line(cpu_num, addr, new_addr, FILL_LLC);

		if(same_page && mshr_threshold > get_l2_mshr_occupancy(cpu_num)){
			//Insert @X in delay queue
			if(dq.valid[dq.tail]){
				int left_index = get_left_index(dq.addr[dq.head]);
				LRR_table[left_index] = dq.addr[dq.head];
				dq.head = (dq.head+1)%ENTRIES_QUEUE;
			}
			dq.addr[dq.tail] = addr;
			dq.cycle[dq.tail] = cpu_cycle + DELAY;
			dq.valid[dq.tail] = 1;
			dq.tail = (dq.tail+1)%ENTRIES_QUEUE;
		}

		// update gauge and rate for every L2 miss
		if(!cache_hit || prefetched){
			gauge += cpu_cycle - last_cycle - rate;
			last_cycle = cpu_cycle;
			if(gauge > GAUGE_MAX){
				gauge = GAUGE_MAX;
				if(rate < RATE_MAX) rate += 1;
			}
			else if(gauge < 0){
				gauge = 0;
				if(rate > 0) rate -= 1;
			}
		}

		// update mshr threshold
		if(rate < bw) mshr_threshold = MSHR_THRESHOLD_2;
		else if(best_score > LOW_SCORE || rate > 2*bw) mshr_threshold = MSHR_THRESHOLD_12;
		else mshr_threshold = 2 + 10*(rate - bw)/bw;
	}
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, int prefetch, unsigned long long int evicted_addr)
{
  // uncomment this line to see the information available to you when there is a cache fill event
  //printf("0x%llx %d %d %d 0x%llx\n", addr, set, way, prefetch, evicted_addr);
	if(prefetch){
		prefetch_bit[set][way] = 1;
		unsigned long long int new_addr = ((addr>>LINE_SIZE)-best_offset)<<LINE_SIZE;
		char same_page = (((new_addr) ^ (addr)) >> 12) == 0; // 4 KB Page
		if(same_page){
			int right_index = get_right_index(new_addr);
			RRR_table[right_index] = new_addr;
		}
	}
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
  printf("Prefetcher heartbeat stats\n");
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
  printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
  printf("Prefetcher final stats\n");
}

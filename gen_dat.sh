#!/bin/bash

echo "#Let the first argument be the folder with the traces to run."
echo "#Let the second argument be the folder with the prefethcers to test"
echo "#Run in dpcsim directory"
echo "#Rows names in rows.txt and columns in columns.txt"

echo "#Run like: ./gen_data.sh traces_dir prefetchers_dir"

rm rows.txt
rm columns.txt

touch rows.txt
for f in $1*.dpc
do
	echo "$f " >> rows.txt
done

touch columns.txt
for f in $2*.c
do
	echo  -ne "$f\t" >> columns.txt
done


STAT_FILE=pref_normal.txt
touch $STAT_FILE
echo "File=$STAT_FILE" > $STAT_FILE

for f in $1*.dpc
do
	for p in $2*.c 
	do	
		gcc -Wall -o dpc2sim $p lib/dpc2sim.a -I./inc
		cat $f | ./dpc2sim  > pout
	   	cat pout | grep complete | awk '{printf $NF}'  >> $STAT_FILE
		echo -ne  "\t" >> $STAT_FILE
	done
	echo  >> $STAT_FILE
done

STAT_FILE=pref_small_llc.txt
touch $STAT_FILE
echo "File=$STAT_FILE" >$STAT_FILE
for f in $1*.dpc
do
	for p in $2*.c 
	do	
		gcc -Wall -o dpc2sim $p lib/dpc2sim.a -I./inc
		cat $f | ./dpc2sim -small_llc  > pout
	   	cat pout | grep complete | awk '{printf $NF}'   >> $STAT_FILE
		echo -ne  "\t" >> $STAT_FILE
	done
	echo >> $STAT_FILE
done

STAT_FILE=pref_low_band.txt
touch $STAT_FILE
echo "File=$STAT_FILE" >$STAT_FILE
for f in $1*.dpc
do
	for p in $2*.c 
	do	
		gcc -Wall -o dpc2sim $p lib/dpc2sim.a -I./inc
		cat $f | ./dpc2sim -low_bandwidth  > pout
	   	cat pout | grep complete | awk '{printf $NF}'   >> $STAT_FILE
		echo -ne  "\t" >> $STAT_FILE
	done
	echo >> $STAT_FILE
done

STAT_FILE=pref_scramble_loads.txt
touch $STAT_FILE
echo "File=$STAT_FILE" >$STAT_FILE
for f in $1*.dpc
do
	for p in $2*.c 
	do	
		gcc -Wall -o dpc2sim $p lib/dpc2sim.a -I./inc
		cat $f | ./dpc2sim -scramble_loads  > pout
	   	cat pout | grep complete | awk '{printf $NF}'   >> $STAT_FILE
		echo -ne  "\t" >> $STAT_FILE
	done
	echo >> $STAT_FILE
done

rm pout

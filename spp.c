/*
 *	Author: Bagdasar Sergiy
 *	Date: 26/4/2017
 *	Email: sergio17949@gmail.com
 *	This prefetcher implements a spatial pattern
 *	predictor. You can find more details in "Accurate and Complexity-Effective Spatial Pattern Prediction" paper.
 */


#include <stdio.h>
#include  <string.h>
#include <stdlib.h>
#include "../inc/prefetcher.h"


#define LINE_SIZE 64
#define PC_LENGTH 54 //lsb
#define SG_LENGTH 12 //lsb
#define PHT_LENGTH 1200
#define CPT_LENGTH 256

int PREFETCH_DEPTH;

enum switch_t {OFF,ON};
enum boolean_t {FALSE,TRUE};
enum status_t{USED,CLEAR};

typedef struct control
{
    int scramble_load;
    int small_llc;
    int low_bandwith;
}control_t;

typedef struct prediction_index
{
    unsigned long long int pc;//PC_LENGTH;
    unsigned long long int sg_offset;//SG_LENGTH;
}pred_index_t; //this would be 8 bytes long.

typedef struct pht_entry
{
    pred_index_t pred_index;
    unsigned int valid:2;
    unsigned long long int pattern;
}pht_entry_t; //this will cost me 20 bytes

typedef struct cpt_entry
{
    unsigned long long int rec_pattern;
    int pht_index;
    unsigned int status:2;
    unsigned long long int tag:54;
}cpt_entry_t; //this would be 20 bytes

typedef struct stats
{
    int pht_hit;
    int pht_probes;
    int pht_entry_hits[PHT_LENGTH];
}stats_t;

int free_pht_index=-1;
int curr_cpt_index=0;
int free_cpt_index=-1;

control_t cflags;
stats_t pht_stats;
pht_entry_t pht[PHT_LENGTH];
cpt_entry_t cpt[CPT_LENGTH];

void l2_prefetcher_initialize(int cpu_num)
{
    printf("Spatial Group Prefetcher Initialization\n");
    int i;
    //initialize input parameters
    cflags.scramble_load=knob_scramble_loads;
    cflags.small_llc=knob_small_llc;
    cflags.low_bandwith=knob_low_bandwidth;
    
    //all the entries in the pht are initially invalid
    for(i=0;i<PHT_LENGTH;i++)
    {
        pht[i].valid=FALSE;
        pht[i].pattern=0;
    }
    for(i=0;i<CPT_LENGTH;i++)
    {
        cpt[i].status=CLEAR;
        cpt[i].tag=0;
        cpt[i].rec_pattern=0;
        cpt[i].pht_index=-1;
    }
    
    pht_stats.pht_hit=0;
    pht_stats.pht_probes=1;
    
    printf("Initialization Complete\n");
}

//assumes little endian, prints a data type in binary
char* printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;
    
    int n=size*8+1;
    char *str=(char*)malloc(sizeof(char)*(n));
    int l=0;
    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            if(byte)
                str[l]='1';
            else
                str[l]='0';
            l++;
        }
    }
    str[n-1]='\0';
    return str;
}

pred_index_t get_pred_index(unsigned long long int ip,
                            unsigned long long int addr)
{
    unsigned long long int one=1;
    pred_index_t pi;
    pi.pc=(ip&((one << PC_LENGTH )-one));
    pi.pc=(pi.pc>>4)<<4;
    pi.sg_offset=(addr&((one << SG_LENGTH )-one));
    return pi;
}

void debug_piv(unsigned long long int addr, unsigned long long int ip)
{
    pred_index_t pred=get_pred_index(ip,addr);
    unsigned long long int piv=((pred.pc<<12)|pred.sg_offset);
    printf("PC=%s\n",printBits(8,&ip));
    printf("AD=%s\n",printBits(8,&addr));
    printf("PV=%s\n",printBits(8,&piv));
}

unsigned long long int set_bit(unsigned long long int var,int pos)
{
    unsigned long long int one=1;
    unsigned long long int ans;
    ans=(var | (one << pos));
    return ans;
}

unsigned long long int unset_bit(unsigned long long int var,int pos)
{
    unsigned long long int one=1;
    unsigned long long int ans=var;
    ans &= ~(one << pos);
    return ans;
}

int in_cpt(unsigned long long int tag)
{
    int i;
    int hit=-1;
    for(i=0;i<CPT_LENGTH;i++)
    {
        if(cpt[i].tag==tag && cpt[i].status==USED)
        {
            hit=i;
            break;
        }
    }
    return hit;
}

int compare_pred_index(pred_index_t p1,pred_index_t p2)
{
    return (p1.pc==p2.pc && p1.sg_offset==p2.sg_offset);
}

int in_pht(pred_index_t pred)
{
    int i;
    int hit=-1;
    for(i=0;i<PHT_LENGTH;i++)
    {
        if(compare_pred_index(pht[i].pred_index,pred)==TRUE &&
           pht[i].valid==TRUE)
        {
            hit=i;
            break;
        }
    }
    return hit;
}

int pick_next_cpt_index()
{
    int loops=0;
    int tmp=free_cpt_index;
    do
    {
        tmp=(tmp+1)%CPT_LENGTH;
        loops++;
    }while(cpt[tmp].status==USED && loops<CPT_LENGTH);
    
    //if couldnt find usused entry
    if(cpt[tmp].status==USED)
    {
        free_cpt_index=(free_cpt_index+1)%CPT_LENGTH;
        tmp=free_cpt_index;
        cpt[tmp].tag=0;
        cpt[tmp].rec_pattern=0;
        cpt[tmp].status=CLEAR;
        cpt[tmp].pht_index=-1;
    }
    return tmp;
}

int pick_next_pht_index()
{
    int loops=0;
    int tmp=free_pht_index;
    do
    {
        tmp=(tmp+1)%PHT_LENGTH;
        loops++;
    }while(pht[tmp].valid==TRUE && loops<PHT_LENGTH);
    
    //if couldnt find unused entry
    if(pht[tmp].valid==TRUE)
    {
        free_pht_index=(free_pht_index+1)%PHT_LENGTH;
        tmp=free_pht_index;
        pht[tmp].pattern=0;
        pht[tmp].valid=FALSE;
    }
    return tmp;
}

void printPredictions(unsigned long long int addr,unsigned long long int ip)
{
    int i;
    int p;
    //if(get_current_cycle(0)>10000) return;
    debug_piv(addr,ip);
    printf("*************************************************************************\n");
    printf("CTP:\n");
    printf("#\tTAG\t\tPATTERN\t\tPHT_INDEX\t\tSTATUS\n");
    for(i=0;i<CPT_LENGTH;i++)
    {
        printf("|%4d\t%32llu\t\t%64s\t\t%4d\t\t%2d|\n",i,cpt[i].tag,
               printBits(8,&cpt[i].rec_pattern),cpt[i].pht_index,cpt[i].status);
    }
    printf("PHT:\n");
    printf("#\tPRED_INDEX\t\tPATTERN\t\tVALID\tHITS\n");
    for(i=0;i<PHT_LENGTH;i++)
    {
        p=i;
        printf("|%4d\t%32llu\t\t%64s\t\t%2u\t%d|\n",i,
               ((pht[p].pred_index.pc<<12)|pht[p].pred_index.sg_offset),
               printBits(8,&pht[p].pattern),pht[p].valid,
               pht_stats.pht_entry_hits[p]);
    }
    printf("************************************************************************\n");
}
int get_access_line(unsigned long long int addr)
{
    unsigned long long int one=1;
    //get the accessed line number within the page
    int line=((addr>>6) & ((one << 6)-one));
    return line;
}

/*
 *This is the SPP feedback mechanism
 * */
void spp_feedback(unsigned long long int addr, unsigned long long int ip,int pht_hit)
{
    int line=get_access_line(addr);
    int new_index;
    
    //a new generation started
    
    //if there is a previous recording
    if(cpt[curr_cpt_index].status==USED)
    {
        //we should copy the currrenly used cpt entry to pht
        pht[cpt[curr_cpt_index].pht_index].pattern=cpt[curr_cpt_index].rec_pattern;
        
        cpt[curr_cpt_index].rec_pattern=0;
        
        //set the valid bit
        pht[cpt[curr_cpt_index].pht_index].valid=TRUE;
    }
    
    //check if there is entry in cpt for this tag
    new_index=in_cpt((addr>>12));
    if(new_index==-1)
    {
        //if not
        
        //pick the next free cpt entry
        new_index=pick_next_cpt_index();
        
        //set the new tag
        cpt[new_index].tag=(addr>>12);
        
        //set the status
        cpt[new_index].status=USED;
    }
    
    //if this prediction index is already there take that entry
    //otherwise a lot of same pred indexes will be floating.
    if(pht_hit!=-1)
    {
        //set the pht index for this cpt entry
        cpt[new_index].pht_index=pht_hit;
        
    }
    else
    {
        //set the allocated pht entry for this cpt entry
        cpt[new_index].pht_index=pick_next_pht_index();
        
        //set the new prediction index
        pht[cpt[new_index].pht_index].pred_index=get_pred_index(ip,addr);
    }
    
    //update the pattern for this access
    cpt[new_index].rec_pattern=set_bit(cpt[new_index].rec_pattern,line);
    
    //set the current cpt index
    curr_cpt_index=new_index;
}

int isSet(unsigned long long int pattern,int index)
{
    unsigned long long int one=1;
    unsigned long long int ans=(pattern & (one << index));
    if(ans==0) return FALSE;
    return TRUE;
}

int density(unsigned long long int *ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int size=8;
    int i, j,s=0;
    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            if(byte)
                s++;
        }
    }
    return s;
}

int roof(double num)
{
    return  (int)(num < 0 ? (num - 0.5) : (num + 0.5));
}

void predict(unsigned long long int addr,unsigned long long int ip,
             unsigned long long int pattern,
             int pht_index)
{
    unsigned long long int page= (addr>>12)<<12;
    unsigned long long int pref_addr=0;
    int bit_index=0,i=0;
    int L2_DEPTH=3;
    //for each set bit prefetch the next PREFETCH_DEPTH lines.
    PREFETCH_DEPTH=density(&pattern)+1;
    if(cflags.low_bandwith==ON)
	{
		if(PREFETCH_DEPTH>1)
			PREFETCH_DEPTH-=1;
		L2_DEPTH=2;
	}
	if(cflags.small_llc==ON)
	{
		//prefetch less line to reduce l2 and l3(inclusive) size
		if(PREFETCH_DEPTH>2)
			PREFETCH_DEPTH=2;
	}
	//PREFETCH_DEPTH=2;
    while(bit_index<LINE_SIZE)
    {
        if(isSet(pattern,bit_index)==TRUE)
        {
            for(i=0;i<PREFETCH_DEPTH;i++)
            {
                pref_addr=(page + ((bit_index+i)<<6));
                if((pref_addr>>12)!=(addr>>12)) continue;
                if(i>L2_DEPTH || get_l2_mshr_occupancy(0)>8)
                {
                    l2_prefetch_line(0,addr,pref_addr,FILL_LLC);
                }
                else
                {
                    l2_prefetch_line(0,addr,pref_addr,FILL_L2);
                }
                pattern=unset_bit(pattern,bit_index+i);
            }
            bit_index+=PREFETCH_DEPTH;
            break;
        }
        bit_index++;
    }
    if(pht_index!=-1)
        pht[pht_index].pattern=pattern;
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr,
                           unsigned long long int ip, int cache_hit)
{
    int i=-1;
    unsigned long long int pattern=0;
    int line=get_access_line(addr);
    
    //find the prediction table for this call
    pred_index_t pred_index=get_pred_index(ip,addr);
    
    printf("Access page: %llu, Pred: %llu\n",(addr>>12)<<12,
           ((pred_index.pc<<12)|pred_index.sg_offset) );
    
    //i=in_pht(pred_index);
    //a new generation begins
    if(cpt[curr_cpt_index].tag!=(addr>>12))
    {
        //update cpt and pht
        spp_feedback(addr,ip,i);
        
        //printPredictions(addr,ip);
    }
    else
    {
        //if the logic tag is the currently recording one
        //update the pattern
        cpt[curr_cpt_index].rec_pattern=set_bit(cpt[curr_cpt_index].rec_pattern,line);
    }
    //capture some stats
    pht_stats.pht_probes++;
    
    //check if this prediction index is already in the pht
    if((i=in_pht(pred_index))!=-1)
    {
        pht_stats.pht_hit++;
        printf("PHT hit!\n");
        pattern=pht[i].pattern;
    }
    else
    {	//if the prediction index is not there
        //the prediction is fetch the next 2 lines
        pattern=set_bit(pattern,line);
        pattern=set_bit(pattern,line+1);
        pattern=set_bit(pattern,line+2);
    }
    
    predict(addr,ip,pattern,i);
    
    
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, 
                   int prefetch, unsigned long long int evicted_addr)
{
    // uncomment this line to see the information available to you when there 
    // is a cache fill event
    //printf("0x%llx %d %d %d 0x%llx\n", addr, set, way, prefetch, evicted_addr);
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
    printf("Prefetcher heartbeat stats\n");
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
    printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
    int i;
    printf("Prefetcher final stats\n");
    printf("PHT Probes=%d, PHT Hits:%d => Perc:%2.f% \n",pht_stats.pht_probes,
           pht_stats.pht_hit,pht_stats.pht_hit*100/pht_stats.pht_probes*1.0);
    for(i=0;i<PHT_LENGTH;i++)
    {
        printf("PHT enrty %d density: %d\n",i,density(&pht[i].pattern));	
    }
}
